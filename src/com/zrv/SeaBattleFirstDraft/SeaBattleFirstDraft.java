package com.zrv.SeaBattleFirstDraft;

import java.util.Random;
import java.util.Scanner;

public class SeaBattleFirstDraft {

    final int[][] SEA;
    int[][] tempGrid = new int[10][10];

    SeaBattleFirstDraft() {
        SEA = new int[10][10];
        randomGen();
    }

    private void randomGen() {
        Random random = new Random();
        for (int i = 0; i < SEA.length; i++) {
            for (int j = 0; j < SEA[i].length; j++) {
                SEA[i][j] = random.nextInt(2);
            }
        }
    }

    public void draw() {
        for (int i = 0; i < SEA.length; i++) {
            for (int j = 0; j < SEA[i].length; j++) {
                if (SEA[i][j] == 0) {      //0 = " ", 1 = "Х"
                    System.out.print(" ");
                } else {
                    System.out.print("X");
                }
            }
            System.out.println();
        }
    }

    public void scaner() {
        while (true) {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter x (1-10): ");
            int i = in.nextInt();
            System.out.println("Enter y (1-10): ");
            int j = in.nextInt();
            try {
                if (i - 1 < SEA.length - 10 || i - 1 >= SEA.length - 1 || j - 1 < SEA.length - 10 || j - 1 >= SEA.length - 1) {
                    throw new SeaBattleFirstDraftException("Try to enter again");
                }
                if (SEA[i - 1][j - 1] == 0) {
                    System.out.println("You are miss!");
                } else {
                    System.out.println("Kill!");
                    SEA[i - 1][j - 1] = 0;
                }
                for (int k = 0; k < SEA.length; k++) {
                    System.arraycopy(SEA[k], 0, tempGrid[k], 0, SEA[k].length);
                }
                for (int l = 0; l < tempGrid.length; l++) {
                    System.arraycopy(tempGrid[l], 0, SEA[l], 0, tempGrid[l].length);
                    System.out.println("__________");
                    draw();
                    break;
                }
            }catch (SeaBattleFirstDraftException e) {
                System.out.println("Исключение перхвачено в теле мотода scaner");
            }
        }
    }
}