package com.zrv.SeaBattleFirstDraft;

    //create a custom exception class that extends the Exception class

public class SeaBattleFirstDraftException extends Exception{
    public SeaBattleFirstDraftException(String message) {        //Constructor that accepts a message
        super(message);
    }
}
